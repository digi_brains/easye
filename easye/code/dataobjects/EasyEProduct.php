<?php

/**
 * Products
 * 
 * @package EasyE
 */
class EasyEProduct extends DataObject {
    
    private static $db = array (
        'ProductDescription'=> 'HTMLText',
        'ProductLive'       => 'Boolean',
        'ProductName'       => 'Varchar(255)',
        'ProductCode'       => 'Varchar(128)',
        'ProductPrice'      => 'Decimal'
    );
    
    private static $has_one = array (
        'ProductImage'      => 'Image'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', CheckboxField::create('ProductLive','Enable this product'));
        $fields->addFieldToTab('Root.Main', TextField::create('ProductName','Product name'));
        $fields->addFieldToTab('Root.Main', TextField::create('ProductPrice','Price without currency symbol (example: 25.99)'));
        $fields->addFieldToTab('Root.Main', TextField::create('ProductCode','Product number'));

        $fields->addFieldToTab('Root.Details', UploadField::create('ProductImage','Product Image'));
        $fields->addFieldToTab('Root.Details', HTMLEditorField::create('ProductDescription','Product Description'));
        return $fields;
    }

    /**
     * Sign individual link inputs to prevent cart tampering on the client side.
     * 
     * With this function, for each product you want to hash, you need to provide the name, value, and code to it.
     * The name and value arguments are the name and value pair of the particular product attribute you're hashing,
     * and the code argument is the product's code value. This function will return an encrypted hash based on
     * the arguments provided.
     *
     * Documentation here: https://wiki.foxycart.com/v/2.0/hmac_validation
     * 
     */
    public function hash($var_name, $var_value, $var_code, $var_parent_code = "", $for_value = false) {
        $config         = SiteConfig::current_site_config(); 
        $api_key        = $config->FoxyApiKey;
        $encodingval    = htmlspecialchars($var_code . $var_parent_code . $var_name . $var_value);
        $label          = ($for_value) ? $var_value : $var_name;
        return $label . '=' . $var_value . '||' . hash_hmac('sha256', $encodingval, $api_key) . ($var_value === "--OPEN--" ? "||open" : "");
    }

}