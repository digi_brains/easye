<?php

/**
 * An extension to add your FoxyCart Private Key to the site settings.
 *
 * @package  EasyE
 */
class EasyEConfig extends DataExtension {

    private static $db = array(
        'FoxyApiKey'    => 'Varchar(128)',
        'FoxyURL'       => 'Varchar(255)'
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab('Root.Main', TextField::create('FoxyApiKey','Your FoxyCart API (v2.0) Key'));
        $fields->addFieldToTab('Root.Main', TextField::create('FoxyURL','Cart URL'));
    }

}