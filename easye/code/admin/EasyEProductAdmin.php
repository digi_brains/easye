<?php

/**
 * Manage products from ModelAdmin
 *
 * @package  EasyE
 */
class EasyEProductAdmin extends ModelAdmin {

    private static $managed_models  = array('EasyEProduct');
    private static $url_segment     = 'products';
    private static $menu_title      = 'Products';
    // private $showImportForm = false;

}