![Alt text](http://prawnstar.net/stuff/eazye.jpg "Eazy-E")
#Easy E is an _easy_ to use, eCommerce, FoxyCart module for SilverStripe Framework/CMS.

**To-Do:** A metric-shit-ton! This module is _very_ basic. Please send me pull requests if you would like to contribute.

##Easy Setup in 5 (or 6) _easy_ steps.

**Requires a [FoxyCart](http://foxycart.com) account and an API key**

(1) Download and extract in your root SilverStripe installation. Run `dev/build?flush=all`

(2) On your page controller add a getter method.
```
    public function getEasyEProduct() {
        return EasyEProduct::get();
    }
```

(3) Log into the CMS and add your API key and store URL to the site Settings tab.

(4) Add a product under the Products tab. At minimum, you must add the Product name, price and a product code and be sure to check the "Enable this product" checkbox.

(5) On any page template you want the product to display add code that looks like this:
```
<% loop EasyEProduct %>
    <% if $ProductLive %>
        <p><a href="https://{$SiteConfig.FoxyURL}/cart?cart=view">view cart</a></p>

        <h1>$ProductName</h1>
        $ProductImage
        <p>$ProductDescription</p>
        <p>
            ${$ProductPrice}<br>
            <a href="https://{$SiteConfig.FoxyURL}?{$hash('name', $ProductName, $ProductCode)}&{$hash('price', $ProductPrice, $ProductCode)}&{$hash('code', $ProductCode, $ProductCode)}">
                Buy
            </a>
        </p>
    <% end_if %>
<% end_loop %>
```

NOTE: The GET parameters in the hash() function in the link. The name and value arguments are the name and value pair of the particular product attribute you're hashing, and the code argument is the product's code value.


(6) *Optional* If you want a fancy AJAX cart you can add this JavaScript (change the YOUR-FOXY-ACCOUNT-NAME to your account name).
```
<!-- FOXYCART -->
    <script src="//cdn.foxycart.com/YOUR-FOXY-ACCOUNT-NAME/loader.js" async defer></script>
<!-- /FOXYCART -->
```